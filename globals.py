import os

class const():

    def __init__(self):
        pass

    HTML_CLASS_VERT_MOD = "vert-mod"
    HTML_CLASS_PROBLEM = "problem"
    HTML_CLASS_WRAPPER_PROBLEM = "wrapper-problem-response"
    HTML_CLASS_BUTTON_SOLUTION = "show problem-action-btn btn-default btn-small"
    HTML_CLASS_FIELD_ANSWER = "field"
    HTML_CLASS_DETAILED_SOLUTION = "detailed-solution"
    HTML_CLASS_MODULE = "nav-item nav-item-chapter"
    HTML_CLASS_ANSWER = "answer"

    HTML_ATTRIBUTE_DATA_ID = "data-id"
    HTML_ATTRIBUTE_CLASS = "class"
    HTML_ATTRIBUTE_ID  = "id"

    DATA_ID_TYPE_PROBLEM = "@problem+block@"
    DATA_ID_TYPE_VIDEO= "@video+block@"
    DATA_ID_HTML_BLOCK = "@html+block@"

    INPUT_TYPE_CHECKBOX = "checkbox"
    INPUT_TYPE_RADIO = "radio"
    INPUT_TYPE_TEXT = "text"

    PATH_GOOGLE_DRIVER = os.path.dirname(os.path.realpath(__file__)) + "\chromedriver"

    URL_EDX_PAGE = "https://courses.edx.org/courses/course-v1:MITx+14.310x+2T2020/courseware/0262349a3edc4951acb4edd327ddcbe8/863dddcc99f041db8e3fef45c5ad22d9"

    ELEMENT_DELAY_SECONDS = 10

    SEQUENCE_VIDEO = "seq_video"
    SEQUENCE_PROBLEM = "seq_problem"

    DB_NAME = "edxScraperDB.db"

    LECTURE_NUMBER = 2

    BOOL_CREATE_TABLES = True

class problemClass():

    def __init__(self, moduleNum, lectureNum, fingerNum, quesNum, edxProblemID, preQuestion):
        self.moduleNum = moduleNum
        self.lectureNum = lectureNum
        self.fingerSection = fingerNum
        self.quesNum = quesNum
        self.preQuestion = preQuestion
        self.question = ""
        self.optionsList = []
        self.answersList = []
        self.textSolution = ""
        self.edxProblemID = edxProblemID
        self.questionType = ""
        self.qID = "M{0}.L{1}.F{2}.Q{3}".format(moduleNum, lectureNum, fingerNum, quesNum)