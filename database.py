import sqlite3
from globals import const as c

def createTables(con):

    cr = con.cursor()
    # Creating the question and long answer table
    cr.execute("CREATE TABLE IF NOT EXISTS tblQuestions("
                    "[qID] text,"
                    "[modNum] integer,"
                    "[lecNum] integer,"
                    "[finNum] integer,"
                    "[quesNum] integer,"
                    "[quesType] text,"
                    "[ques] text,"
                    "[longAns] text);")

    cr.execute("CREATE TABLE IF NOT EXISTS tblAnswers("
                    "[qID] text,"
                    "[modNum] integer,"
                    "[lecNum] integer,"
                    "[finNum] integer,"
                    "[quesNum] integer,"
                    "[optNum] integer,"
                    "[option] text,"
                    "[isAns] boolean);")

def populateQUESTIONS(con, problemDict):

    cr = con.cursor()

    sqlStr = ""
    for (dictElem, theClass) in problemDict.items():

        sqlStr = "INSERT INTO tblQuestions (qID, modNum, lecNum, finNum, quesNum, quesType, ques, longAns) " \
                  "VALUES ('{0}', {1}, {2}, {3}, {4}, '{5}', '{6}', '{7}')".format(theClass.qID,
                                                                                      theClass.moduleNum,
                                                                                      theClass.lectureNum,
                                                                                      theClass.fingerSection,
                                                                                      theClass.quesNum,
                                                                                      theClass.questionType,
                                                                                      theClass.preQuestion.replace("'", "''") + " " + theClass.question.replace("'", "''"),
                                                                                      theClass.textSolution.replace("'", "''"))

        print("Inserting into tblQuestions: {0}".format(sqlStr))
        cr.execute(sqlStr)
        con.commit()

def populateANSWERS(con, problemDict):

    cr = con.cursor()

    sqlStr = ""
    for (dictElem, theClass) in problemDict.items():

        for (i, opt) in enumerate(theClass.optionsList):

            sqlStr = "INSERT INTO tblAnswers (qID, modNum, lecNum, finNum, quesNum, optNum, option, isAns) " \
                      "VALUES ('{0}', {1}, {2}, {3}, {4}, '{5}', '{6}', '{7}')".format(theClass.qID,
                                                                                       theClass.moduleNum,
                                                                                       theClass.lectureNum,
                                                                                       theClass.fingerSection,
                                                                                       theClass.quesNum,
                                                                                       i+1,
                                                                                       theClass.optionsList[i].replace("'", "''"),
                                                                                       theClass.answersList[i].replace("'", "''"))

            print("Inserting into tblAnswers: {0}".format(sqlStr))
            cr.execute(sqlStr)
            con.commit()

def insertIntoDatabase(problemDict):

    con = sqlite3.connect(c.DB_NAME)

    if c.BOOL_CREATE_TABLES:
        createTables(con)

    populateQUESTIONS(con, problemDict)
    populateANSWERS(con, problemDict)

if __name__ == "__main__":

    # view database

    con = sqlite3.connect(c.DB_NAME)
    cr = con.cursor()
    #cr.execute("SELECT name FROM sqlite_master WHERE type='table';")
    cr.execute("select * from tblQuestions")
    rows = cr.fetchall()
    print(rows)