from globals import const as c
import sqlite3
import random
import string

def getQuestions(con):

    cr = con.cursor()
    cr.execute("SELECT * FROM tblQuestions")

    return cr.fetchall()

def getOptions(con, qID):

    cr = con.cursor()
    cr.execute("SELECT * FROM tblAnswers "\
               "WHERE qID = '{0}'".format(qID))

    return cr.fetchall()


if __name__ == "__main__":

    con = sqlite3.connect(c.DB_NAME)
    quesList = getQuestions(con)
    alphaList = string.ascii_uppercase
    score = 0
    iter = 0

    while True:

        boolRight = True
        iter += 1

        # Grab random question
        r = random.randrange(1, len(quesList))

        # Grab answers
        optList = getOptions(con = con, qID = quesList[r][0])
        
        percScore = 0 if iter == 1 else round(score /(iter-1) * 100)
    
        print("_____________________________")
        print("<< QUESTION: {0}; SCORE: {1}% >>".format(iter, percScore ))
        print("=============================")
        print("QUESTION: {0}: {1}".format(quesList[r][0], quesList[r][6]))
        print("=============================")

        # Multiple answers
        if quesList[r][5] != c.INPUT_TYPE_TEXT:

            if quesList[r][5] == c.INPUT_TYPE_CHECKBOX:
                print("Select all correct answers: (A, B, ...)")
            elif quesList[r][5] == c.INPUT_TYPE_RADIO:
                print("Select the single answer: (A, B, ...)")

            # Print options
            for i in range(0, len(optList)):
                print("{0}: {1}".format(alphaList[i], optList[i][6]))

            # Get inputs
            inpList = [x.strip() for x in input().split(",")]

            if inpList[0].lower() == "bad":
                iter -= 1
                continue

            # Get answers
            ansList = []
            for i in range(0, len(optList)):
                if optList[i][7] == "TRUE":
                    ansList.append(alphaList[i])

            # Check input
            for ans in ansList:
                if ans not in inpList:
                    boolRight = False
                    break

            # Give score
            if boolRight:
                score += 1
                print("CORRECT!")
            else:
                print("WRONG!")

            # Display answer
            print("Answer is: {0}".format(ansList))
            print("Explanation: {0}".format(quesList[r][7]))
            print("")
            print("Press any key to move on...")
            input()

        # Text answer
        elif quesList[r][5] == c.INPUT_TYPE_TEXT:

            print("Type the answer")
            inpList = [x.strip() for x in input().split(",")]
            
            if inpList[0].lower() == "bad":
                iter -= 1
                continue
                
            # Deal with fractions
            if "/" in optList[0][6]:
                ans = float(optList[0][6].split("/")[0])/float(optList[0][6].split("/")[1])
            else:
                ans = float(optList[0][6])
            
            if abs((float(inpList[0])/ans)-1) > 0.03:
                boolRight = False
                
            if boolRight:
                score += 1
                print("CORRECT!")
            else:
                print("WRONG!")

            # Display answer
            print("Answer is: {0}".format(ans))
            print("Explanation: {0}".format(quesList[r][7]))
            print("")
            print("Press any key to move on...")
            input()
            


