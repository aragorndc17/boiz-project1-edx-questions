
from selenium import webdriver # Libraries to scrape the web
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.common.keys import Keys

import argparse # Library to pass arguments when executing Python code
import getpass  # Library to capture password

from database import *
from globals import const as c
from globals import problemClass

def checkIfXPathExists(xpath):

    try:
        webdriver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True

def waitForPageToLoad(idToSearch="", tagToSearch=""):

    delay = c.ELEMENT_DELAY_SECONDS  # seconds

    try:
        WebDriverWait(driver, delay).until(EC.presence_of_element_located((idToSearch, tagToSearch)))
        return True
        #myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((idToSearch, tagToSearch)))
    except TimeoutException:
        print("Loading took too much time for {0}: {1}".format(idToSearch, tagToSearch))
        return False

def getChromeDriver(useRemote = False, cmdExec = "", sessionID = ""):

    # Use active window
    if useRemote:
        driver = webdriver.Remote(command_executor=cmdExec, desired_capabilities={})
        driver.close()  # This closes dummy browser
        driver.session_id = sessionID

    # Open new window
    else:
        driver = webdriver.Chrome(c.PATH_GOOGLE_DRIVER)
        print(driver.command_executor._url) # http://127.0.0.1:62606
        print(driver.session_id) # c3f322bcbf3e766e60f73126ff288a13

    return driver

def loginToEdx(usr, pwd):

    driver.get(c.URL_EDX_PAGE)
    waitForPageToLoad(By.ID, "login-email")
    driver.find_element_by_id("login-email").send_keys(usr)
    driver.find_element_by_id("login-password").send_keys(pwd)
    driver.find_element_by_xpath("//button[@class='action action-primary action-update js-login login-button']").click()
    waitForPageToLoad(By.CLASS_NAME, c.HTML_CLASS_VERT_MOD)

def getTerminalArguments():

    parser = argparse.ArgumentParser(description= "edX Scraper")
    parser.add_argument("-usr", required=True, action="store")
    return parser.parse_args()

def getDataIDList(driver):

    dataIDElems = driver.find_elements_by_xpath("*//div[@class='{0}']/*".format(c.HTML_CLASS_VERT_MOD))
    return [x.get_attribute(c.HTML_ATTRIBUTE_DATA_ID) for x in dataIDElems]

def getPreQuestion(driver, id):

    xPath = "*//div[@data-id='{0}']".format(id)
    return driver.find_element_by_xpath(xPath).text.replace("\n", " ")

def clickSolutionButton(driver):

    xPath = "//*[@id='problem_{0}']/*//button[@class='{1}']".format(edxProblemID, c.HTML_CLASS_BUTTON_SOLUTION)
    buttonElement = driver.find_element_by_xpath(xPath)

    if buttonElement.get_attribute("disabled") == None:
        buttonElement.send_keys(Keys.RETURN)

    return waitForPageToLoad(By.ID, "{0}_solution_1".format(edxProblemID))

def getQuestion(driver, problemID):

    xPath = "*//div[@id='problem_{0}']/*//div[@class='{1}']/p".format(problemID, c.HTML_CLASS_WRAPPER_PROBLEM)
    return " ".join([x.text.strip("\n") for x in driver.find_elements_by_xpath(xPath)]).replace("\n", " ")

def getQuestionType(driver):

    # This gets the list of inputs types - and we take the first option and will use this

    xPath = "*//div[@data-id='{0}']/*//div[@class='{1}']/*//input".format(id, c.HTML_CLASS_PROBLEM)
    typeElements = driver.find_elements_by_xpath(xPath)

    if len(typeElements) > 0:
        return [x.get_attribute("type") for x in typeElements][0]

def getOptionAnswers(driver):

    # Get both options, and whether it is right or wrong

    xPath = "*//div[@data-id='{0}']/*//div[@class='{1}']/*//div[@class='{2}']/*//div[@class='{3}']/label".format(id,
                                                                                                                 c.HTML_CLASS_PROBLEM,
                                                                                                                 c.HTML_CLASS_WRAPPER_PROBLEM,
                                                                                                                 c.HTML_CLASS_FIELD_ANSWER)
    elemOptions = driver.find_elements_by_xpath(xPath)

    ansList = []

    for opt in elemOptions:
        if "choicegroup_correct" in opt.get_attribute("class"):
            ansList.append("TRUE")
        else:
            ansList.append("FALSE")

    return ansList

def getOptions(driver):

    # Get both options, and whether it is right or wrong

    xPath = "*//div[@data-id='{0}']/*//div[@class='{1}']/*//div[@class='{2}']/*//div[@class='{3}']/label".format(id,
                                                                                                                 c.HTML_CLASS_PROBLEM,
                                                                                                                 c.HTML_CLASS_WRAPPER_PROBLEM,
                                                                                                                 c.HTML_CLASS_FIELD_ANSWER)
    elemOptions = driver.find_elements_by_xpath(xPath)

    optList = []

    for opt in elemOptions:
        optList.append(opt.text.replace("\n", " "))

    return optList

def getLongSolution(driver, quesClass, solutionLoaded):

    # If a solution was loaded....
    if solutionLoaded:

        xPath = "*//div[@data-id='{0}']/*//div[@class='{1}']/*//div[@class='{2}']".format(id,
                                                                                          c.HTML_CLASS_PROBLEM,
                                                                                          c.HTML_CLASS_DETAILED_SOLUTION)

        elems = driver.find_elements_by_xpath(xPath)

        # Weird instance where <detailed solution> is used instead of <detailed-solution>
        if len(elems) == 0:
            xPath2 = xPath.replace("detailed-solution", "detailed solution")
            quesClass.textSolution = driver.find_element_by_xpath(xPath2).text.replace("\n", " ")
        else:
            quesClass.textSolution = driver.find_element_by_xpath(xPath).text.replace("\n", " ")

        # If the questionType is text - then capture the text answers into lists appropriately
        if quesClass.questionType == c.INPUT_TYPE_TEXT:

            xPath = "*//div[@data-id='{0}']/*//div[@class='{1}']/*//p[@class='{2}']".format(id,
                                                                                            c.HTML_CLASS_PROBLEM,
                                                                                            c.HTML_CLASS_ANSWER)

            quesClass.optionsList.append(driver.find_element_by_xpath(xPath).text)
            quesClass.answersList.append("TRUE")

    # If a solution was not loaded then will assume there is no solution
    else:
        quesClass.textSolution = "No solution"

if __name__ == "__main__":

    # Get user information from terminal
    termArgs = getTerminalArguments()
    user = termArgs.usr
    pwd = getpass.getpass()

    # <useRemote = True> means we use an open window, assuming we know its cmdExec and sessionID
    driver = getChromeDriver(useRemote = True,
                             cmdExec = "http://127.0.0.1:50394",
                             sessionID = "63245764a24475d320b903ae3fe52771")

    #loginToEdx(user, pwd)
    #exit()

    # Get module number
    modNum = driver.find_element_by_xpath("*//span[@class='{0}']".format(c.HTML_CLASS_MODULE)).text.split(":")[0].split()[-1]
    lectureNum = c.LECTURE_NUMBER # Still need to automate this - for now let it be a constant that is manually changed
    fingerNum = 0

    # Dictionary to hold problems
    problemDict = {}

    # Loop through the entire lecture
    elemsSequence = driver.find_elements_by_xpath("*//ol[@id='sequence-list']/li/button")

    # Loop through the sequence of videos and problems
    for elem in elemsSequence:

        quesNum = 0

        # If the sequence is a problem...
        if c.SEQUENCE_PROBLEM in elem.get_attribute("class"):

            # Click on the button
            elem.send_keys(Keys.RETURN)

            # Wait until the questions have loaded
            while True:
                if elem.get_attribute("class") == "seq_problem nav-item tab active":
                    break

            fingerNum += 1

            # Get the "data IDs" within the problem page, which represent sections of information
            dataIdList = getDataIDList(driver=driver)

            preQuestion = ""

            for id in dataIdList:

                # Assuming we only have 1 of these per question: Store pre-question info and attach to every problem
                if c.DATA_ID_HTML_BLOCK in id:
                    preQuestion = getPreQuestion(driver=driver, id = id)

                # Problem blocks
                if c.DATA_ID_TYPE_PROBLEM in id:

                    quesNum += 1

                    # Get our qID - store into Dictionary and initialise problemClass
                    quesID = "M{0}.L{1}.F{2}.Q{3}".format(modNum, lectureNum, fingerNum, quesNum)

                    # Get edxQID
                    edxProblemID = id.split("@")[-1]
                    problemDict[quesID] = problemClass(modNum, lectureNum, fingerNum, quesNum, edxProblemID, preQuestion)

                    # Get question
                    problemDict[quesID].question = getQuestion(driver=driver, problemID = edxProblemID)

                    ############ NEED TO DO A BETTER JOB HERE #########################################################################

                    # Work here for <center> in <p>. Sometimes the question has more information
                    xPath = "*//div[@data-id='{0}']/*//div[@class='{1}']/*//div[@class='{2}']/center".format(id,
                                                                                                        c.HTML_CLASS_PROBLEM,
                                                                                                        c.HTML_CLASS_WRAPPER_PROBLEM)

                    questElems = driver.find_elements_by_xpath(xPath)
                    if len(questElems) > 0:
                        problemDict[quesID].question = problemDict[quesID].question + " " + \
                                                       " ".join([x.text for x in driver.find_elements_by_xpath(xPath)]).replace("\n", " ")

                    ####################################################################################################################

                    # Get question type
                    problemDict[quesID].questionType = getQuestionType(driver)

                    # Get options (need a separate function to capture information before clicking the solution button)
                    # As there is additional text next to options when the solution is picked
                    problemDict[quesID].optionsList= getOptions(driver=driver)

                    # Click on solution button and record effect
                    solutionLoaded = clickSolutionButton(driver=driver)

                    # Get whether an option was right or wrong
                    problemDict[quesID].answersList = getOptionAnswers(driver = driver)

                    # Get long solution
                    getLongSolution(driver = driver, quesClass = problemDict[quesID], solutionLoaded = solutionLoaded)

    # for (dictElem, theClass) in problemDict.items():
    #     print("QuestionID: {0}\n"
    #           "Question Type: {1}\n"
    #           "Question: {2}\n"
    #           "Potential Answers: {3}\n"
    #           "IsSolution: {4}\n"
    #           "longSolution: {5}".format(dictElem, theClass.questionType,
    #                                      theClass.preQuestion + "\n" + theClass.question,
    #                                      theClass.optionsList, theClass.answersList, theClass.textSolution))

    insertIntoDatabase(problemDict)